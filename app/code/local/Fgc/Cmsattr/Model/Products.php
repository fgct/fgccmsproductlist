<?php
class Fgc_Cmsattr_Model_Products extends Mage_Catalog_Model_Product
{
    public $_collection;

    public function getItemsCollection($att_name, $att_value = null, $page = 1, $limit=40) {
        $this->_collection = $this->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter($att_name, array('eq' => $att_value))
            /* ->setPageSize($limit)
            ->setCurPage($page) */;
        Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($this->_collection);
        return $this->_collection;
    }

    public function setPageSize(int $limit) {
        $this->_collection->setPageSize($limit);
        return $this->_collection;
    }

    public function setCurPage(int $page) {
        $page = $page < 1 ? 1 : $page;
        $this->_collection->setCurPage($page);
        return $this->_collection;
    }

    public function setOrder($attribute, $direction = 'asc') {
        if(!in_array(strtolower($direction), ['asc', 'desc']))
            $direction = 'asc';
            
        $this->_collection->setOrder($attribute, $direction);
        return $this->_collection;
    }

}