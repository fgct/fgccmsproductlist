<?php

// Mage_Catalog_Block_Product_Abstract
// Mage_Catalog_Block_Category_View

class Fgc_Cmsattr_Block_Category_View extends Mage_Catalog_Block_Category_View {

    protected function _prepareLayout() {
        parent::_prepareLayout();

        if (!$this->getTemplate()) {
            $this->setTemplate('fgc/cmsattr/catalog/category/view.phtml');
        }
        return $this;
    }

    public function getProductListHtml() {
        if (!$this->getData('product_list_html')) {
            $html = $this->getLayout()->createBlock('fgc_cmsattr/product_list')
                //->setTemplate('fgc/cmsattr/catalog/product/list.phtml')
                ->setTemplate('catalog/product/list.phtml')
                ->setData($this->getData())
                ->toHtml();
            $this->setData('product_list_html', $html);
        }
        return $this->getData('product_list_html');
    }

    public function getCurrentAttribute() {
        $attributeCode = $this->getAttName();
        $att_value = $this->getAttValue();

        $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode(Mage_Catalog_Model_Product::ENTITY, $attributeCode);
        $attributeModel->setData('value', $att_value);
        return $attributeModel;
    }

    public function getCurrentCategory() {
        return Mage::getModel('catalog/category')->load(2);
    }

}