<?php

// Mage_Catalog_Block_Product_Abstract
// Mage_Catalog_Block_Category_View

class Fgc_Cmsattr_Block_Product_List extends Mage_Catalog_Block_Product_List {

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        if (!$this->getTemplate()) {
            $this->setTemplate('fgc/cmsattr/catalog/product/list.phtml');
        }
        return $this;
    }

    public function getLoadedProductCollection()
    {
        $att_name = $this->getAttName();
        $att_value = $this->getAttValue();
        if (!$att_name)
            return false;

        switch ($att_value) {
            case 'yes':
                $att_value = 1;
                break;
            case 'no':
                $att_value = 0;
                break;
            
            default:
                break;
        }

        $limit = (int) Mage::app()->getRequest()->getParam('limit', 40);
        $page = (int) Mage::app()->getRequest()->getParam('p', 1);
        $order = Mage::app()->getRequest()->getParam('order', 'position');
        $direction = Mage::app()->getRequest()->getParam('dir', 'asc');

        if (is_null($this->_itemCollection)) {
            $this->_itemCollection = Mage::getModel('fgc_cmsattr/products')
                ->getItemsCollection($att_name, $att_value);
                
            $this->_itemCollection->setPageSize($limit);
            $this->_itemCollection->setCurPage($page);
            $this->_itemCollection->setOrder($order, $direction);
        }
        $total = $this->_itemCollection->count();
        return $this->_itemCollection;
    }

    /**
     * Retrieve list toolbar HTML
     *
     * @return string
     */
    public function getToolbarHtml() {
        $items = $this->getLoadedProductCollection();

        $toolbar = Mage::app()->getLayout()->createBlock('catalog/product_list_toolbar');
        $toolbar->setCollection($items);
        // $toolbar->setShowAmounts(false);
        // $toolbar->setShowPerPage(false);

        $pager = Mage::getSingleton('core/layout')->createBlock('page/html_pager')
        ->setCollection($items);
        $toolbar->setChild('product_list_toolbar_pager', $pager);
        $html = $toolbar->toHtml();
        return $html;
    }

}