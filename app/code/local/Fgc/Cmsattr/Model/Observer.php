<?php
class Fgc_Cmsattr_Model_Observer {

    /**
     * Event: controller_action_layout_render_before_cms_page_view
     */
    function addItem($observer) {
        $event = $observer->getEvent();
        $page = Mage::getSingleton('cms/page');

        if($page->getContent() && preg_match('/fgc_cmsattr/i', $page->getContent())) {
            if($head = Mage::app()->getLayout()->getBlock('head')) {
                $head->addItem('skin_js', '../../em0140/default/js/em_category.js');
                $head->addItem('skin_css', '../../em0140/em0140_supermarket/css/style_supermarket.css');
                // $js_url = Mage::getDesign()->getSkinUrl('js/em_category.js');
                // $head->addItem('em_js', $js_url);
            }
            
            if ($root = Mage::app()->getLayout()->getBlock('root')) {
                $root->addBodyClass('catalog-category-view');
            }
        }

        return;
    }
}