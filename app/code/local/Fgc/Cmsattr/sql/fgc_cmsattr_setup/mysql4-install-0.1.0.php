<?php 

/** @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

try {
    $installer->getConnection()->insertMultiple(
        $installer->getTable('admin/permission_block'),
        array(
            array('block_name' => 'fgc_cmsattr/category_view', 'is_allowed' => 1),
        )
    );
} catch(Exception $e) {}
$installer->endSetup();
